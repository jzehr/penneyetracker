PennController.ResetPrefix(null) // Shorten command names (keep this)

Template( row =>
  newTrial(
    // This tracker considers a element look at if 15% of the gazes out of 30 cycles fall on that element
    newEyeTracker("tracker", 20, 0.15)
    ,
    newButton("Start")
        .print()
        .wait( getEyeTracker("tracker").test.ready().failure(
            newText("warning", "You must grant this page access to your webcam before you can proceed.").print()
        ) )
        .remove()
    ,
    getText("warning").remove(),
    fullscreen()    // Go fullscreen after click on Start
    ,
    getEyeTracker("tracker")
        .calibrate(60, 2)  // Calibrate phase, validate if 60% accuracy, allows for 2 attempts
        .train()    // Keep training the model after the calibration phase
    ,
    newCanvas("screen", "100vw", "100vh")
        .add( "center at 25%" , "middle at 25%" , newCanvas("topleft", "40vw", "40vh") )
        .add( "center at 75%" , "middle at 25%" , newCanvas("topright", "40vw", "40vh") )
        .add( "center at 25%" , "middle at 75%" , newCanvas("bottomleft", "40vw", "40vh") )
        .add( "center at 75%" , "middle at 75%" , newCanvas("bottomright", "40vw", "40vh") )
        .print( "center at 50vw" , "middle at 50vh" )
    ,
    // We successively place this button on the 4 areas to train the model on those regions
    newButton("Click here")
        .size("10vw", "10vh")
        .print( "left at 0%" , "top at 0%" , getCanvas("topleft") ).wait()
        .print( "right at 100%" , "top at 0%" , getCanvas("topright") ).wait()
        .print( "left at 0%" , "bottom at 100%" , getCanvas("bottomleft") ).wait()
        .print( "right at 100%" , "bottom at 100%" , getCanvas("bottomright") ).wait()
        .print( "center at 50%" , "center at 50%" ).wait().remove() // Make sure last gaze is at the center
    ,
    getCanvas("topleft").css("background-color",row.TopLeftColor),        // Color each of the 4 areas
    getCanvas("topright").css("background-color",row.TopRightColor),
    getCanvas("bottomleft").css("background-color",row.BottomLeftColor),
    getCanvas("bottomright").css("background-color",row.BottomRightColor)
    ,
    getEyeTracker("tracker")
        .stopTraining()     // Stop training the model (= it's ok if the participant's eyes and mouse are no longer in sync)
        .add( getCanvas("topleft") , getCanvas("topright") , getCanvas("bottomleft") , getCanvas("bottomright") )
        .log()
        .start()    // Start recording the participant's gazes
    ,
    newSelector().add(getCanvas("topleft"), getCanvas("topright"), getCanvas("bottomleft"), getCanvas("bottomright")).log().wait()
    ,
    getCanvas("screen").remove(),
    getEyeTracker("tracker").stop() // Prevent from recording any unnecessary gazes
  )
)